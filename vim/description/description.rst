

.. index::
   pair: Description ; VIm


.. _vim_description:


=================
VIm Description
=================

.. contents::
   :depth: 3



En français
=============

.. seealso::

   - https://fr.wikipedia.org/wiki/Vim

Vim est un éditeur de texte, c’est-à-dire un logiciel permettant la
manipulation de fichiers texte.

Il est directement inspiré de vi (un éditeur très répandu sur les systèmes
d’exploitation de type UNIX), dont il est le clone le plus populaire.

Son nom signifie d’ailleurs Vi IMproved, que l’on peut traduire par
**VI aMélioré**.

Vim est un éditeur de texte extrêmement personnalisable, que ce soit
par l'ajout d'extensions, ou par la modification de son fichier de
configuration, écrits dans son propre langage d'extension, le Vim script.

Malgré de nombreuses fonctionnalités, il conserve un temps de démarrage
court (même agrémenté d'extensions) et reste ainsi adapté pour des
modifications simples et ponctuelles (de fichiers de configuration
par exemple).

Vim se différencie de la plupart des autres éditeurs par son fonctionnement
modal, hérité de vi.
En effet, il possède trois modes:

- le mode normal (dans lequel vous êtes lorsque Vim démarre),
- le mode commande,
- et le mode édition.

Vim est un logiciel libre. Son code source a été publié pour la
première fois en 1991 par Bram Moolenaar, son principal développeur.

Depuis, ce dernier a continué de l’améliorer, avec l’aide de nombreux
contributeurs.


English
=============

.. seealso::

   - https://github.com/vim/vim

What is Vim ?
----------------

Vim is a greatly improved version of the good old UNIX editor Vi.

Many new features have been added: multi-level undo, syntax highlighting,
command line history, on-line help, spell checking, filename completion,
block operations, script language, etc.

There is also a Graphical User Interface (GUI) available.

Still, Vi compatibility is maintained, those who have Vi "in the fingers"
will feel at home. See runtime/doc/vi_diff.txt for differences with Vi.

This editor is very useful for editing programs and other plain text
files. All commands are given with normal keyboard characters, so those
who can type with ten fingers can work very fast.

Additionally, function keys can be mapped to commands by the user, and
the mouse can be used.

Vim runs under MS-Windows (NT, 2000, XP, Vista, 7, 8, 10), Macintosh,
VMS and almost all flavours of UNIX.

Porting to other systems should not be very difficult. Older versions
of Vim run on MS-DOS, MS-Windows 95/98/Me, Amiga DOS, Atari MiNT,
BeOS, RISC OS and OS/2. These are no longer maintained.
