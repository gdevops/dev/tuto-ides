

.. index::
   pair: Macros ; VIm


.. _vim_macros:

==================
VIm macros
==================


.. contents::
   :depth: 3



Commenter avec #
==================

::

    let @s="i#"


Décommenter
==================

::

    let @t="x"


::

    let @s="i#"
    let @t="x"
