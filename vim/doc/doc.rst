

.. index::
   pair: Documentation ; VIm


.. _vim_documentation:

==================
VIm documentation
==================

.. seealso::

   - http://vim.wikia.com/wiki/Vim_documentation


.. contents::
   :depth: 3
