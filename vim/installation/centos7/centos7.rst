

.. index::
   pair: VIm Installation ; CentOS 7


.. _vim8_install_centos7:

===============================
VIm 8 installation on Centos7
===============================


.. seealso::

    - https://www.systutorials.com/241762/how-to-upgrade-vim-to-version-8-on-centos-7/


.. contents::
   :depth: 3



curl -L https://copr.fedorainfracloud.org/coprs/mcepl/vim8/repo/epel-7/mcepl-vim8-epel-7.repo -o /etc/yum.repos.d/mcepl-vim8-epel-7.repo
==========================================================================================================================================


::

    curl -L https://copr.fedorainfracloud.org/coprs/mcepl/vim8/repo/epel-7/mcepl-vim8-epel-7.repo -o /etc/yum.repos.d/mcepl-vim8-epel-7.repo

::

	[@id3_programs-staging ~]# curl -L https://copr.fedorainfracloud.org/coprs/mcepl/vim8/repo/epel-7/mcepl-vim8-epel-7.repo -o /etc/yum.repos.d/mcepl-vim8-epel-7.repo
	  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
									 Dload  Upload   Total   Spent    Left  Speed
	100   306  100   306    0     0    357      0 --:--:-- --:--:-- --:--:--   357


::

	[@id3_programs-staging ~]# yum update vim*

::

	Modules complémentaires chargés : fastestmirror, langpacks
	Repository base is listed more than once in the configuration
	Repository centosplus is listed more than once in the configuration
	Repository extras is listed more than once in the configuration
	Repository updates is listed more than once in the configuration
	base                                                                                                                                                                                                                  | 3.6 kB  00:00:00
	epel                                                                                                                                                                                                                  | 4.7 kB  00:00:00     extras                                                                                                                                                                                                                | 3.4 kB  00:00:00     mcepl-vim8                                                                                                                                                                                                            | 3.9 kB  00:00:00
	updates                                                                                                                                                                                                               | 3.4 kB  00:00:00
	(1/6): epel/group_gz                                                                                                                                                                                                  | 266 kB  00:00:00
	(2/6): epel/updateinfo                                                                                                                                                                                                | 906 kB  00:00:00
	(3/6): epel/primary_db                                                                                                                                                                                                | 6.3 MB  00:00:00
	(4/6): extras/7/x86_64/primary_db                                                                                                                                                                                     | 185 kB  00:00:00
	(5/6): mcepl-vim8/x86_64/primary_db                                                                                                                                                                                   |  31 kB  00:00:02
	(6/6): updates/7/x86_64/primary_db                                                                                                                                                                                    | 6.9 MB  00:00:04
	Determining fastest mirrors
	 * extras: centos.mirrors.benatherton.com
	 * updates: mirrors.atosworldline.com
	Résolution des dépendances
	--> Lancement de la transaction de test
	---> Le paquet vim-common.x86_64 2:7.4.160-2.el7 sera mis à jour
	---> Le paquet vim-common.x86_64 2:8.0.1601-1.0.94.el7.centos sera utilisé
	---> Le paquet vim-enhanced.x86_64 2:7.4.160-2.el7 sera mis à jour
	---> Le paquet vim-enhanced.x86_64 2:8.0.1601-1.0.94.el7.centos sera utilisé
	---> Le paquet vim-filesystem.x86_64 2:7.4.160-2.el7 sera mis à jour
	---> Le paquet vim-filesystem.x86_64 2:8.0.1601-1.0.94.el7.centos sera utilisé
	---> Le paquet vim-minimal.x86_64 2:7.4.160-2.el7 sera mis à jour
	---> Le paquet vim-minimal.x86_64 2:8.0.1601-1.0.94.el7.centos sera utilisé
	--> Résolution des dépendances terminée

	Dépendances résolues

	=============================================================================================================================================================================================================================================
	 Package                                                  Architecture                                     Version                                                                Dépôt                                                Taille============================================================================================================================================================================================================================================$Mise à jour :
	 vim-common                                               x86_64                                           2:8.0.1601-1.0.94.el7.centos                                           mcepl-vim8                                           6.7 M
	 vim-enhanced                                             x86_64                                           2:8.0.1601-1.0.94.el7.centos                                           mcepl-vim8                                           1.3 M
	 vim-filesystem                                           x86_64                                           2:8.0.1601-1.0.94.el7.centos                                           mcepl-vim8                                            34 k
	 vim-minimal                                              x86_64                                           2:8.0.1601-1.0.94.el7.centos                                           mcepl-vim8                                           522 k

	Résumé de la transaction
	=============================================================================================================================================================================================================================================
	Mettre à jour  4 Paquets

	Taille totale des téléchargements : 8.5 M
	Is this ok [y/d/N]: y
	Downloading packages:
	Delta RPMs disabled because /usr/bin/applydeltarpm not installed.
	attention : /var/cache/yum/x86_64/7/mcepl-vim8/packages/vim-enhanced-8.0.1601-1.0.94.el7.centos.x86_64.rpm: Entête V3 RSA/SHA1 Signature, clé ID 41f0d8a8: NOKEY                                           ] 451 kB/s | 2.4 MB  00:00:13 ETA
	La clé publique pour vim-enhanced-8.0.1601-1.0.94.el7.centos.x86_64.rpm n'est pas installée
	(1/4): vim-enhanced-8.0.1601-1.0.94.el7.centos.x86_64.rpm                                                                                                                                                             | 1.3 MB  00:00:05
	(2/4): vim-filesystem-8.0.1601-1.0.94.el7.centos.x86_64.rpm                                                                                                                                                           |  34 kB  00:00:00
	(3/4): vim-minimal-8.0.1601-1.0.94.el7.centos.x86_64.rpm                                                                                                                                                              | 522 kB  00:00:01
	(4/4): vim-common-8.0.1601-1.0.94.el7.centos.x86_64.rpm                                                                                                                                                               | 6.7 MB  00:00:23
	---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	Total                                                                                                                                                                                                        374 kB/s | 8.5 MB  00:00:23
	Récupération de la clé à partir de https://copr-be.cloud.fedoraproject.org/results/mcepl/vim8/pubkey.gpg
	Importation de la clef GPG 0x41F0D8A8 :
	ID utilisateur : « mcepl_vim8 (None) <mcepl#vim8@copr.fedorahosted.org> »
	Empreinte      : c1eb 8063 5e04 ce7c 213b eba5 3eda 1a9f 41f0 d8a8
	Provient de    : https://copr-be.cloud.fedoraproject.org/results/mcepl/vim8/pubkey.gpg
	Est-ce correct [o/N] : o
	Running transaction check
	Running transaction test
	Transaction test succeeded
	Running transaction
	  Mise à jour  : 2:vim-filesystem-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                      1/8
	  Mise à jour  : 2:vim-common-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                          2/8
	  Mise à jour  : 2:vim-enhanced-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                        3/8
	  Mise à jour  : 2:vim-minimal-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                         4/8
	  Nettoyage    : 2:vim-enhanced-7.4.160-2.el7.x86_64                                                                                                                                                                                     5/8
	  Nettoyage    : 2:vim-common-7.4.160-2.el7.x86_64                                                                                                                                                                                       6/8   Nettoyage    : 2:vim-filesystem-7.4.160-2.el7.x86_64
	  Vérification : 2:vim-minimal-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                         1/8
	  Vérification : 2:vim-common-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                          2/8   Vérification : 2:vim-filesystem-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                      3/8   Vérification : 2:vim-enhanced-8.0.1601-1.0.94.el7.centos.x86_64                                                                                                                                                                        4/8
	  Vérification : 2:vim-minimal-7.4.160-2.el7.x86_64                                                                                                                                                                                      5/8
	  Vérification : 2:vim-enhanced-7.4.160-2.el7.x86_64                                                                                                                                                                                     6/8
	  Vérification : 2:vim-filesystem-7.4.160-2.el7.x86_64                                                                                                                                                                                   7/8
	  Vérification : 2:vim-common-7.4.160-2.el7.x86_64                                                                                                                                                                                       8/8

	Mis à jour :
	  vim-common.x86_64 2:8.0.1601-1.0.94.el7.centos           vim-enhanced.x86_64 2:8.0.1601-1.0.94.el7.centos           vim-filesystem.x86_64 2:8.0.1601-1.0.94.el7.centos           vim-minimal.x86_64 2:8.0.1601-1.0.94.el7.centos

	Terminé !



vim --version
===============

::

    [@id3_programs-staging ~]# vim --version

::

	VIM - Vi IMproved 8.0 (2016 Sep 12, compiled Mar 13 2018 12:09:38)
	Rustines incluses : 1-1601
	Modifié par <bugzilla@redhat.com>
	Compilé par <bugzilla@redhat.com>
	Énorme version sans interface graphique.
	  Fonctionnalités incluses (+) ou non (-) :
	+acl               +farsi             +mouse_sgr         -tag_any_white
	+arabic            +file_in_path      -mouse_sysmouse    -tcl
	+autocmd           +find_in_path      +mouse_urxvt       +termguicolors
	-autoservername    +float             +mouse_xterm       +terminal
	-balloon_eval      +folding           +multi_byte        +terminfo
	+balloon_eval_term -footer            +multi_lang        +termresponse
	-browse            +fork()            -mzscheme          +textobjects
	++builtin_terms    +gettext           +netbeans_intg     +timers
	+byte_offset       -hangul_input      +num64             +title
	+channel           +iconv             +packages          -toolbar
	+cindent           +insert_expand     +path_extra        +user_commands
	-clientserver      +job               +perl/dyn          +vertsplit
	-clipboard         +jumplist          +persistent_undo   +virtualedit
	+cmdline_compl     +keymap            +postscript        +visual
	+cmdline_hist      +lambda            +printer           +visualextra
	+cmdline_info      +langmap           +profile           +viminfo
	+comments          +libcall           +python/dyn        +vreplace
	+conceal           +linebreak         -python3           +wildignore
	+cryptv            +lispindent        +quickfix          +wildmenu
	+cscope            +listcmds          +reltime           +windows
	+cursorbind        +localmap          +rightleft         +writebackup
	+cursorshape       +lua/dyn           +ruby/dyn          -X11
	+dialog_con        +menu              +scrollbind        -xfontset
	+diff              +mksession         +signs             -xim
	+digraphs          +modify_fname      +smartindent       -xpm
	-dnd               +mouse             +startuptime       -xsmp
	-ebcdic            -mouseshape        +statusline        -xterm_clipboard
	+emacs_tags        +mouse_dec         -sun_workshop      -xterm_save
	+eval              +mouse_gpm         +syntax
	+ex_extra          -mouse_jsbterm     +tag_binary
	+extra_search      +mouse_netterm     +tag_old_static
			 fichier vimrc système : "/etc/vimrc"
		 fichier vimrc utilisateur : "$HOME/.vimrc"
	 2me fichier vimrc utilisateur : "~/.vim/vimrc"
		  fichier exrc utilisateur : "$HOME/.exrc"
	 fichier de valeurs par défaut : "$VIMRUNTIME/defaults.vim"
				   $VIM par défaut : "/etc"
			$VIMRUNTIME par défaut : "/usr/share/vim/vim80"
	Compilation : gcc -c -I. -Iproto -DHAVE_CONFIG_H     -O2 -g -pipe -Wall -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches   -m64 -mtune=generic -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -D
	_FORTIFY_SOURCE=1
	Édition de liens : gcc   -L. -Wl,-z,relro -fstack-protector -rdynamic -Wl,-export-dynamic -Wl,--enable-new-dtags -Wl,-rpath,/usr/lib64/perl5/CORE  -Wl,-z,relro  -L/usr/local/lib -Wl,--as-needed -o vim        -lm -lnsl  -lselinux  -lncurses -lacl -lattr -lgpm -ldl   -Wl,--enable-new-dtags -Wl,-rpath,/usr/lib64/perl5/CORE  -fstack-protector  -L/usr/lib64/perl5/CORE -lperl -lresolv -lnsl -ldl -lm -lcrypt -lutil -lpthread -lc
	[@id3_programs-staging ~]#
