

.. index::
   pair: IDE ; VIm


.. _vim:

==================
VIm
==================

.. seealso::

   - https://www.vim.org/
   - https://github.com/vim/vim


.. figure:: ../_static/Vimlogo.svg.png
   :align: center

   Vim Logo


.. toctree::
   :maxdepth: 3

   description/description
   doc/doc
   books/books
   tips/tips
   macros/macros
   installation/installation
   versions/versions
