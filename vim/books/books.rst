

.. index::
   pair: Mastering Vim Quickly ; Book VIm


.. _vim_books:

==================
VIm books
==================

.. seealso::

   - http://vim.wikia.com/wiki/Vim_documentation


.. contents::
   :depth: 3


Mastering Vim Quickly: From WTF to OMG in no time
===================================================


.. seealso::

   - https://jovicailic.org/mastering-vim-quickly/
