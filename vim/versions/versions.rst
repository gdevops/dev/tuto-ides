

.. index::
   pair: Versions ; VIm


.. _vim_versions:

==================
VIm versions
==================

.. seealso::

   - https://github.com/vim/vim


.. contents::
   :depth: 3



.. _vim8_1:

VIm 8.1.0 (2018-05-17)
========================

.. seealso::

   - https://www.vim.org/vim-8.1-released.php
   - https://github.com/vim/vim/tree/v8.1.0000

Vim 8.1 is a minor release, a few new features have been added since
Vim 8.0. A lot of bugs have been fixed, documentation was updated, etc.
The main new feature is the terminal window.

This builds on top of the asynchronous features added in Vim 8.0.
The terminal window can be used for many purposes, here are a few examples:

- Run a command, such as "make", while continuing to edit in other windows.
  The progress of the command can be observed, the terminal window is
  continuously updated.
- Run a shell where you can execute a series of commands.
- Use the new terminal debugger plugin for debugging inside Vim.
  This is especially useful over an ssh connection, when opening other
  terminals is not possible or impractical. I use this to fix problems
  in Vim when travelling.

The terminal window is also used in tests, to grab a screenshot and
compare it with the expected state.
This allows for testing interactive actions, such as the popup menu.


.. _vim8:

VIm 8.0.0 (2016-09-12)
========================


.. seealso::

   - https://github.com/vim/vim/tree/v8.0.0000
