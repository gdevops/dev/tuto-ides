.. index::
   pair: Vim; Tips7
   pair: Vim; replacement


.. _vim_tips7:

=============================================================
Tips7 : replacement
=============================================================


.. contents::
   :depth: 3

Introduction
=============


::

	Sujet : 	Mastering Vim Quickly #7
	Date : 	Tue, 05 Jun 2018 15:54:44 +0000
	De : 	Jovica <contact@jovicailic.org>
	Pour : 	patrick.vergain@id3.eu


Hello Patrick,

Let's talk about ranges a little more.
As already mentioned, for most commands, the default range
is . (the current line).
However, for :g (global) and :w (write) commands the default
is % (all lines).

Learning by example is very effective, so here's a few:

Replacement in the whole file
===============================


Let’s say your task is to replace all occurrences of the string with
new string in the whole file. This is a very common use case.

Here’s how you do it::

    %s/old_string/new_string/g

- %s means that substitution will be performed for all the lines.
- g specifies that the action will be performed over all occurrences
  in the line.

If the g flag were not used in the command above, only the first
occurrence in the line would be replaced.

Replacement in range: line after current line to end of a file
===============================================================

::

    :.+1,$s/bad/good/g

Replacement in range: current to current+5 line, inclusive
=============================================================

::

    :.,.+4s/bad/good/g

Replacement in range: between patterns a and b, inclusive
===========================================================

::

    :?a?,/b/s/bad/good
