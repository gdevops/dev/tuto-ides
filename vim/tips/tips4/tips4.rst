.. index::
   pair: Vim; Tips4
   pair: Vim; goto


.. _vim_navigate:

=============================================================
Tips4 : You need to be good at navigating around your files
=============================================================

Advanced Vim users prefer to keep their fingers around the home row on a keyboard.
This is possible, because in Vim you can use keys h, j, k and l for navigation.

It might be hard to get used to these at start.
I wrote about the best way to remember them in Mastering Vim Quickly.

Vim has many commands for navigation, but here are few of the most useful ones:

- gg - Go to the top of the file
- G - Go to the bottom of the file
- % - Go to the matching pair of (), [], {}
- :NUM - Go to line NUM. :28 jumps to line 28

Let's see why some of these are so useful:

- In order to fix indentation in your entire file, you can run command::

      gg=G

- If you want to fix indentation on one code block, you can hit::

      =%

  standing on an opening bracket.

- Or if you want to fix indentation in next 15 lines below your current line,
  you can run::

      15==


As you can guess, = is an operator which formats the code based on the
configured formatting rules.

Together with navigation commands, it gives you a lot of power.

In fact, there's a whole language behind Vim.
