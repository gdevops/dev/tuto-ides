.. index::
   pair: Vim; Tips5


.. _vim_tips5:

=============================================================
Tips5 : on files
=============================================================

I'm sure you already know that using command :e <filename> you can open
a file in your existing Vim session.

But, did you know Vim has a :[r]ead command?
You can use it to insert a file, or the output from a system command, into the current buffer.

Here are few powerful examples:

- Insert the file test.txt below the cursor in your current buffer::

      :r test.txt

- Insert the file test.txt before the first line in your current buffer::

      :0r test.txt

- Insert lines 2 to 8 from a file test.txt below the cursor::

      :r!sed -n 2,8p test.txt

- Insert a directory listing below the cursor::

      :r !ls
