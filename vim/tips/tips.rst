

.. index::
   pair: Tips ; VIm


.. _vim_tips:

==================
VIm Tips
==================

.. seealso::

   - http://vim.wikia.com/wiki/Vim_Tips_Wiki


.. toctree::
   :maxdepth: 3

   tips4/tips4
   tips5/tips5
   tips7/tips7
   tips8/tips8
