.. index::
   pair: Vim; Tips8
   pair: Vim; persistent-undo
   pair: Vim; dot-command
   pair: Vim; first-vim-session


.. _vim_tips8:

=======================================================================
Tips8 : vim-persistent-undo + vim-the-dot-command + first-vim-session
=======================================================================

.. seealso:

   - https://jovicailic.org/2017/04/vim-persistent-undo/
   - https://jovicailic.org/2018/03/vim-the-dot-command/
   - https://jovicailic.org/2018/02/first-vim-session/

.. contents::
   :depth: 3

Introduction
=============


::

	Sujet : 	Mastering Vim Quickly #8
	Date : 	Tue, 12 Jun 2018 15:42:43 +0000
	De : 	Jovica <contact@jovicailic.org>



Hey Patrick,

I hope you're doing well!

Here are some of my blog posts you might find interesting:

- `Persistent Undo in Vim`_
- `The "dot" command in Vim`_
- and for total beginners - `Your First Vim Session`_


.. _`Persistent Undo in Vim`:  https://jovicailic.org/2017/04/vim-persistent-undo/
.. _`The "dot" command in Vim`: https://jovicailic.org/2018/03/vim-the-dot-command/
.. _`Your First Vim Session`:  https://jovicailic.org/2018/02/first-vim-session/
