.. IDEs tutorial documentation master file, created by
   sphinx-quickstart on Fri Apr  6 11:39:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/dev/tuto-ides/rss.xml>`_


.. _tuto_ide:

====================
**Tuto IDEs**
====================

.. toctree::
   :maxdepth: 6

   news/news
   vim/vim
   vscodium/vscodium
   vscode/vscode
   xed/xed
   tools/tools
