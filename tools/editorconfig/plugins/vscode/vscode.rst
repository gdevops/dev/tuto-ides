

.. index::
   pair: Editorconfig ; vscode

.. _editorconfig_vscode_plugin:

============================
Editorconfig vscode plugin
============================

.. seealso::

   - https://github.com/editorconfig/editorconfig-vscode
