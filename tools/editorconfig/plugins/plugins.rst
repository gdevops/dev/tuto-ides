

.. index::
   pair: Editorconfig ; plugins

.. _editorconfig_plugins:

=========================
Editorconfig plugins
=========================

.. toctree::
   :maxdepth: 3

   core_js/core_js
   vim/vim
   vscode/vscode
