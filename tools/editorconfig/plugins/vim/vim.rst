

.. index::
   pair: Editorconfig ; vim

.. _editorconfig_vim_plugin:

=========================
Editorconfig vim plugin
=========================

.. seealso::

   - https://github.com/editorconfig/editorconfig-vim
