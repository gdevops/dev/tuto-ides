

.. index::
   pair: Editorconfig ; core-js

.. _editorconfig_core_js_plugin:

============================
Editorconfig core-js plugin
============================

.. seealso::

   - https://github.com/editorconfig/editorconfig-core-js
