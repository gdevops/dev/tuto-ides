

.. index::
   pair: Editorconfig ; properties

.. _editorconfig_properties:

=========================
Editorconfig properties
=========================

.. seealso::

   - https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties
