

.. index::
   pair: Editorconfig ; Tool
   ! Editorconfig

.. _editorconfig:

==================
Editorconfig
==================

.. seealso::

   - https://editorconfig.org/
   - https://github.com/editorconfig/
   - https://github.com/editorconfig/editorconfig
   - https://x.com/EditorConfig

.. figure:: logo_editorconfig.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   properties/properties
   plugins/plugins
