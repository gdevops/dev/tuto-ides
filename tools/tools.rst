.. index::
   pair: IDE ; Tools
   ! Tools

.. _ides_tools:

==================
Tools
==================

.. toctree::
   :maxdepth: 3

   editorconfig/editorconfig
   pre_commit/pre_commit
