

.. index::
   pair: Tool ; pre-commit
   pair: Python ; pre-commit
   ! pre-commit

.. _ides_pre_commit_tool:

==================================================================================================
**pre-commit** : a Python framework for managing and maintaining multi-language pre-commit hooks
==================================================================================================

