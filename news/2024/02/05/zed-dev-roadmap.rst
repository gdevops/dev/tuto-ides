.. index::
   pair: IDE ; zed.dev

.. _zed_dev_2024_02_05:

=========================================
2024-02-05 https://zed.dev/roadmap
=========================================

- https://zed.dev/roadmap


Adoption
===========

Goal: Understand what developers want and give it to them

Lead: @maxbrunsfeld

Roadmap
==========

- Extensibility

    - Themes
    - Language
    - Plugins

- Linux Support
- Web support
- Git

    - Diffs
    - Commit
    - Stage
    - History

