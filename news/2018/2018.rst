

.. index::
   pair: IDEs ; 2018


.. _ides_2018:

==================
IDEs news 2018
==================

Ten years of Vim/
==================

- https://matthias-endler.de/2018/ten-years-of-Vim/
- https://x.com/matthiasendler
- https://github.com/mre/dotVim/blob/master/.vimrc
- http://vim.wikia.com/wiki/Using_tab_pages
- https://github.com/VSCodeVim/Vim
