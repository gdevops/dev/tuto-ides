
.. index::
   pair: Installation; vscodium

.. _vscodium_install:

================================================================================
vscodium installation
================================================================================


.. seealso::

   - https://www.fossmint.com/vscodium-clone-of-visual-studio-code-for-linux/
   - https://vscodium.com/#migrate

.. toctree::
   :maxdepth: 6

   debian/debian
