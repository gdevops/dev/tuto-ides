

.. _vscodium_install_debian:

================================================================================
Debian-based vscodium installation
================================================================================


.. seealso::

   - https://www.fossmint.com/vscodium-clone-of-visual-studio-code-for-linux/
   - https://vscodium.com/#migrate


.. contents::
   :depth: 3

Introduction
=============

Follow these steps to install VSCodium on any Debian-based distro like Ubuntu.

Take note of the pipe "|" symbol used to join the commands.

Add its repo’s GPG key
========================

::

    wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -


Add the repo to your system
==============================

::

    echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list


Update your PC and install the app
====================================

::

    sudo apt update && sudo apt install vscodium

::

    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Tous les paquets sont à jour.
    Lecture des listes de paquets... Fait
    Construction de l'arbre des dépendances
    Lecture des informations d'état... Fait
    Le paquet suivant a été installé automatiquement et n'est plus nécessaire :
    mint-backgrounds-tara
    Veuillez utiliser « sudo apt autoremove » pour le supprimer.
    Les NOUVEAUX paquets suivants seront installés :
    vscodium
    0 mis à jour, 1 nouvellement installés, 0 à enlever et 0 non mis à jour.
    Il est nécessaire de prendre 48,5 Mo dans les archives.
    Après cette opération, 195 Mo d'espace disque supplémentaires seront utilisés.
    Réception de :1 https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs vscodium/main amd64 vscodium amd64 1.33.1-1555005058 [48,5 MB]
    48,5 Mo réceptionnés en 19s (2 519 ko/s)
    Sélection du paquet vscodium précédemment désélectionné.
    (Lecture de la base de données... 582451 fichiers et répertoires déjà installés.)
    Préparation du dépaquetage de .../vscodium_1.33.1-1555005058_amd64.deb ...
    Dépaquetage de vscodium (1.33.1-1555005058) ...
    Traitement des actions différées (« triggers ») pour mime-support (3.60ubuntu1) ...
    Traitement des actions différées (« triggers ») pour desktop-file-utils (0.23+linuxmint4) ...
    Traitement des actions différées (« triggers ») pour bamfdaemon (0.5.3+18.04.20180207.2-0ubuntu1) ...
    Rebuilding /usr/share/applications/bamf-2.index...
    Paramétrage de vscodium (1.33.1-1555005058) ...
    Traitement des actions différées (« triggers ») pour gnome-menus (3.13.3-11ubuntu1.1) ...



If you like, you can transfer your tools and preferences from VS Code to
VSCodium using the instruction manual here_.

.. _here: https://vscodium.com/#migrate
