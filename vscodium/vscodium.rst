
.. index::
   pair: IDE; vscodium

.. _vscodium:

================================================================================
vscodium
================================================================================

.. seealso::

   - https://github.com/VSCodium/vscodium
   - https://vscodium.com
   - https://vscodium.com/#migrate
   - https://github.com/VSCodium/vscodium-docker-files


.. figure:: logo_vscodium.png
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 6

   installation/installation
   versions/versions
