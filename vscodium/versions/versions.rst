
.. index::
   pair: vscodium ; Versions

.. _vscodium_versiosn:

================================================================================
vscodium versions
================================================================================

.. seealso::

   - https://github.com/VSCodium/vscodium/releases

.. toctree::
   :maxdepth: 3

   1.33.1/1.33.1
