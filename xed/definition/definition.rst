
.. _xed_def:

================
xed definition
================

.. contents::
   :depth: 3

English wikipedia definition
=============================

.. seealso::

   - https://en.wikipedia.org/wiki/Xed


Xed is a light weight text editor forked from Pluma and is the default text
editor in Linux Mint.

Xed is a graphical application which supports editing multiple text files in
one window via tabs. It fully supports international text through its use of
the Unicode UTF-8 encoding.

As a general purpose text editor, Xed supports most standard editor features,
and emphasizes simplicity and ease of use.

Its core feature set includes syntax highlighting of source code, auto indentation,
and printing support with print preview.


Xed github  definition
========================

.. seealso::

   - https://github.com/linuxmint/xed/blob/master/README

xed is a small and lightweight text editor.

xed supports most standard editing features, plus several not found in your
average text editor (plugins being the most notable of these).

Although new features are always under development, currently xed has:

* Complete support for UTF-8 text
* Syntax highlighting
* Support for editing remote files
* Search and Replace
* Printing and Print Previewing Support
* File Revert
* A complete preferences interface
* Configurable Plugin system, with optional python support

Some of the plugins, packaged and installed with xed include, among others:

* Word count
* Spell checker
* Change case of selected text
* File Browser
* Sort
* Insert Date/Time
* Tag list

xed is released under the GNU General Public License (GPL) version 2, see
the file 'COPYING' for more information.

Installation
--------------

Simple install procedure::

    % meson . build				# run the `configure' script
    % ninja -v -C build		# build xed
    [ Become root if necessary ]
    % ninja install -v  -C  build	# install xed
