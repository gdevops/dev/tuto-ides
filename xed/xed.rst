.. index::
   pair: Editor ; xed
   ! xed


.. _xed:

================================================================================
xed
================================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/Xed
   - https://github.com/linuxmint/xed


.. toctree::
   :maxdepth: 6

   definition//definition
   versions/versions
