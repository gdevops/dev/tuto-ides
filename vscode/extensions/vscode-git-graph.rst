
.. index::
   pair: VScode extension; vscode-git-graph

.. _vscodegit_graph:

==================================================================================================================================
**vscode-git-graph** View a Git Graph of your repository in Visual Studio Code, and easily perform Git actions from the graph
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph
- https://github.com/mhutchie/vscode-git-graph
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#git-graph

::

    ext install mhutchie.git-graph
