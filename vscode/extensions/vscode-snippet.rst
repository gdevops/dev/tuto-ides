
.. index::
   pair: VScode extension; snippet

.. _snippet:

================================================================================
**vscode snippet**
================================================================================

- https://marketplace.visualstudio.com/items?itemName=vscode-snippet.Snippet
- https://github.com/mre/vscode-snippet
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#snippet


Le copilot du pauvre. Cette extension permet de rechercher parmi les cheat
page de la communauté.

Par exemple vous chercher un bout de code permettant de parser du json en
python. [CTRL] + [SHIFT] + [P]

