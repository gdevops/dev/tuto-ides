
.. index::
   pair: Vscode extension; codeium

.. _codeium:

==================================================================================================================================
**Codeium** Free AI-powered code acceleration toolkit
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=Codeium.codeium
- https://codeium.com/faq
- https://codeium.com/playground

What is Codeium ?
=====================

Codeium is the modern coding superpower, a free code acceleration toolkit
built on cutting edge AI technology. Currently, Codeium provides autocomplete,
chat, and search capabilities in 70+ languages, with lightning fast speeds
and state-of-the-art suggestion quality.

There are many parts of the modern coding workflow that are boring,
tedious, or frustrating, from regurgitating boilerplate to poring through
StackOverflow. Recent advances in AI allow us to eliminate these parts, making
it seamless to turn your ideas into code. With easy integration into Visual
Studio Code and a less than 2 minute installation process, you can focus on
being the best software developer, not the best code monkey.

With Codeium, you get:

- Unlimited single and multi-line code completions forever
- IDE-integrated chat: no need to leave VSCode to ChatGPT, and use
  convenient suggestions such as Refactor and Explain
- Support for 70+ programming languages: Javascript, Python, Typescript,
  PHP, Go, Java, C, C++, Rust, Ruby, and more.
- Support through our Discord Community

Join thousands of other developers who are using Codeium for free to accelerate
their software iteration process, improve their code quality and consistency,
reduce the number of code review iterations, accelerate developer onboarding,
and keep them in their flow state. Want to learn more? Check out our FAQ.
