
.. index::
   pair: VScode extension; gitlab-workflow

.. _gitlab_workflow:

================================================================================
**gitlab-workflow**
================================================================================

- https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/
- https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#gitlab-workflow


::

    ext install GitLab.gitlab-workflow
