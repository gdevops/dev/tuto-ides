
.. index::
   pair: VScode extension; tree-extended

.. _tree_extended:

================================================================================
**tree-extended**
================================================================================

- https://marketplace.visualstudio.com/items?itemName=rulyotano.tree-extended
- https://github.com/rulyotano/tree-extended-vscode
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#tree-extended


::

    ext install rulyotano.tree-extended
