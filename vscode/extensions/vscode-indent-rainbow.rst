
.. index::
   pair: Vscode extensions; vscode-indent-rainbow

.. _vscode_indent_rainbow:

==================================================================================================================================
**vscode-indent-rainbow** Extension which shows indentation with a faint rainbow colored background to make them more readable
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow
- https://github.com/oderwat/vscode-indent-rainbow


Description
=====================

This extension colorizes the indentation in front of your text, alternating
four different colors on each step.

Some may find it helpful in writing code for Python, Nim, Yaml, and
probably even filetypes that are not indentation dependent.



