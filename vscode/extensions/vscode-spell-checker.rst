
.. index::
   pair: Vscode extension; vscode-spell-checker

.. _vscode_spell_checker:

==================================================================================================================================
**vscode-spell-checker** A simple source code spell checker for code
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker
- https://github.com/oderwat/vscode-spell-checker


Description
=====================

A basic spell checker that works well with code and documents.

The goal of this spell checker is to help catch common spelling errors
while keeping the number of false positives low.



