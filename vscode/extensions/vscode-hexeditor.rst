
.. index::
   pair: Vscode extension; vscode-hexeditor

.. _vscode_hexeditor:

==================================================================================================================================
**vscode-hexeditor** Free AI-powered code acceleration toolkit
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=ms-vscode.hexeditor
- https://github.com/microsoft/vscode-hexeditor


Description
=====================

A custom editor extension for Visual Studio Code which provides a hex editor
for viewing and manipulating files in their raw hexadecimal representation.

Features
==========

- Opening files as hex
- A data inspector for viewing the hex values as various different data types
- Editing with undo, redo, copy, and paste support
- Find and replace

