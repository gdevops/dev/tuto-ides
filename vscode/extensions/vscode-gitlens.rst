
.. index::
   pair: VScode extension; vscode-gitlens

.. _vscode_gitlens:

==================================================================================================================================
**vscode-gitlens** Visualize code authorship at a glance
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens
- https://github.com/gitkraken/vscode-gitlens


Description
==============

GitLens is a powerful open-source extension for Visual Studio Code.

GitLens supercharges your Git experience in VS Code. Maintaining focus is
critical, extra time spent context switching or missing context disrupts your
flow. GitLens is the ultimate tool for making Git work for you, designed to
improve focus, productivity, and collaboration with a powerful set of tools
to help you and your team better understand, write, and review code.

GitLens sets itself apart from other Git tools through its deep level of
integration, versatility, and ease of use. GitLens sits directly within your
editor, reducing context switching and promoting a more efficient workflow. We
know Git is hard and strive to make it as easy as possible while also going
beyond the basics with rich visualizations and step-by-step guidance and safety,
just to name a few.
