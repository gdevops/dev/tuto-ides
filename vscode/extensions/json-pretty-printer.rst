
.. index::
   pair: VScode extension; json pretty printer

.. _json_pretty_printer:

================================================================================
**json pretty printer**
================================================================================

- https://marketplace.visualstudio.com/items?itemName=euskadi31.json-pretty-printer
- https://github.com/euskadi31/vscode-json-pretty-printer
