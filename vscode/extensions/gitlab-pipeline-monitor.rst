
.. index::
   pair: VScode extension; gitlab-pipeline monitor

.. _gitlab_pipeline_monitor:

================================================================================
**gitlab-pipeline monitor**
================================================================================

- https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#gitlab-workflow


::

    ext install balazs4.gitlab-pipeline-monitor
