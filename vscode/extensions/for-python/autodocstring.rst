.. index::
   pair: autodocstring; Python

.. _autodocstring:

================================================================================
**autodocstring** VSCode extension that generates docstrings for python files
================================================================================

- https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring
- https://github.com/NilsJPWerner/autoDocstring
- https://github.com/NilsJPWerner

Features
==========

- Quickly generate a docstring snippet that can be tabbed through.
- Choose between several different types of docstring formats.
- Infers parameter types through pep484 type hints, default values, and var names.
- Support for args, kwargs, decorators, errors, and parameter types

