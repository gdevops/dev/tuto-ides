
.. index::
   pair: ruff; vscode
   pair: ruff; format
   pair: ruff; check

.. _ruff_vscode:

==================================================================================================================================
**ruff-vscode** (A Visual Studio Code extension with support for the Ruff linter) |ruff|
==================================================================================================================================

- https://github.com/astral-sh/ruff-vscode
- https://github.com/astral-sh/ruff-vscode/commits.atom
- https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff
- https://docs.astral.sh/ruff/
- https://docs.astral.sh/ruff/linter/ (ruff check)
- https://docs.astral.sh/ruff/formatter/ (ruff format)

Installation
=================

Suivez ces étapes :

- Accédez à l'onglet "Extensions" (ou utilisez le raccourci Ctrl+Shift+X).
- Recherchez "XXX" dans la barre de recherche des extensions.


::

    ext install ruff-vscode


Description
==============

A Visual Studio Code extension for Ruff, an extremely fast Python linter and
code formatter, written in Rust.

Available on the `Visual Studio Marketplace https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff <https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff>`_.

Ruff can be used to replace Flake8 (plus dozens of plugins), Black, isort,
pyupgrade, and more, all while executing tens or hundreds of times faster
than any individual tool.


Interested in using Ruff with another editor? Check out `ruff-lsp https://github.com/astral-sh/ruff-lsp <https://github.com/astral-sh/ruff-lsp>`_.


Usage
======

Once installed in Visual Studio Code, ruff will automatically execute when
you open or edit a Python or Jupyter Notebook file.

If you want to disable Ruff, you can `disable this extension https://code.visualstudio.com/docs/editor/extension-marketplace#_disable-an-extension <https://code.visualstudio.com/docs/editor/extension-marketplace#_disable-an-extension>`_ per workspace in Visual Studio Code.


Configuration
================

- https://docs.astral.sh/ruff/configuration/
