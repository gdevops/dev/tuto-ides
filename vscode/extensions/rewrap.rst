
.. index::
   pair: VScode extension; rewrap

.. _rewrap:

================================================================================
**rewrap**
================================================================================

- https://marketplace.visualstudio.com/items?itemName=stkb.rewrap
- https://github.com/stkb/Rewrap
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/#rewrap


Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter::

    ext install stkb.rewrap
