
.. index::
   pair: Extensions; vscode

.. _vscode_extensions:

================================================================================
**vscode extensions**
================================================================================

- https://blog.stephane-robert.info/docs/visual-studio-code/#installation-et-gestion-des-extensions
- https://blog.stephane-robert.info/post/vscode-favorite-extensions/


Suivez ces étapes :

- Accédez à l'onglet "Extensions" (ou utilisez le raccourci Ctrl+Shift+X).
- Recherchez "XXX" dans la barre de recherche des extensions.


.. toctree::
   :maxdepth: 3

   codeium
   json-pretty-printer
   rewrap
   ruff-vscode
   tree-extended
   vscode-ansible
   vscode-git-graph
   vscode-gitlens
   vscode-hexeditor
   vscode-indent-rainbow
   vscode-markdown-preview-enhanced
   gitlab-pipeline-monitor
   gitlab-workflow
   vscode-remote-ssh
   vscode-spell-checker
   vscode-snippet
   for-python/for-python

