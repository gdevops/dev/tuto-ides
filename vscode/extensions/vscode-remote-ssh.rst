
.. index::
   pair: Vscode extensions; vscode-remote-ssh

.. _vscode_remote_ssh:

==================================================================================================================================
**vscode-remote-ssh**
==================================================================================================================================

- https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow
- https://github.com/microsoft/vscode-remote-release
- https://blog.stephane-robert.info/post/visual-code-remote/

::

    ext install ms-vscode-remote.remote-ssh


Commentaire de Stéphane Robert
===================================

Je vous ai déjà partagé cette extension dans ce billet parlant du développement
à distance via les extensions remote de Microsoft.

Personnellement, je l'utilise sur mon poste de travail Windows en me connectant
dans une VM Linux construite avec Vagrant sous Hyper-V.

Cela me permet aussi d'ouvrir rapidement mes projets via son navigateur.


